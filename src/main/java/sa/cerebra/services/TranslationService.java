package sa.cerebra.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sa.cerebra.DTOs.TranslationDTO;
import sa.cerebra.repos.TranslationRepository;

import java.io.IOException;

@Service
public class TranslationService {
    @Autowired
    private GoogleTranslate translate;

    @Autowired
    private TranslationRepository translationRepository;

    public TranslationDTO translate(TranslationDTO sourceText) throws IOException {
        String result = translate.translate(sourceText.getFrom(),
                sourceText.getTo(),
                sourceText.getTargetText());
        TranslationDTO resultDTO = TranslationDTO.builder().
                from(sourceText.getFrom()).
                to(sourceText.getTo()).
                targetText(sourceText.getTargetText()).
                translatedText(result).
                build();

        return TranslationDTO.fromDAO(translationRepository.save(TranslationDTO.fromIbmTranslationDTO(resultDTO)));
    }

    public void deleteTranslation(Integer id) {
        translationRepository.deleteById(id);

    }
}
