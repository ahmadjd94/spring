package sa.cerebra.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import sa.cerebra.DTOs.TranslationDTO;
import sa.cerebra.services.TranslationService;

import java.io.IOException;

@RestController
public class TranslationController {
    @Autowired
    private TranslationService translationService;


    @ResponseBody
    @PostMapping("/translate")
    @CrossOrigin(origins = "http://localhost:4200")
    public TranslationDTO translate(@RequestBody TranslationDTO translationDTO)  throws IOException {
        return translationService.translate(translationDTO);
    }

    public void deleteTransation(@PathVariable Long id) {
//        translationService.deleteTranslation(id);
    }
}
