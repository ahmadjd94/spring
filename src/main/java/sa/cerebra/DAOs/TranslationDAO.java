package sa.cerebra.DAOs;

import lombok.Builder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Builder
@Entity(name="translation")
public class TranslationDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String sourceText;
    private String translatedText;

    public String getSourceText() {
        return sourceText;
    }
    public String getTranslatedText(){
        return translatedText;
    }

    public void setSourceText(String text){
        this.sourceText = text;
    }
    public void setTranslatedText(String text){
        this.translatedText = text;
    }
}
