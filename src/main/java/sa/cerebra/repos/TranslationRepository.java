package sa.cerebra.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sa.cerebra.DAOs.TranslationDAO;

@Repository
public interface TranslationRepository extends CrudRepository<TranslationDAO,Integer> {
}
