package sa.cerebra.DTOs;

import lombok.Builder;
import sa.cerebra.DAOs.TranslationDAO;

@Builder
public class TranslationDTO {
    private String targetText;
    private String from;
    private String to;
    private String translatedText;

    public TranslationDTO(String targetText, String from, String to, String translatedText) {
        this.targetText = targetText;
        this.from = from;
        this.to = to;
        this.translatedText = translatedText;
    }

    public TranslationDTO() {

    }

    public String getTargetText() {
        return targetText;
    }

    public void setTargetText(String targetText) {
        this.targetText = targetText;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    public static TranslationDTO fromDAO(TranslationDAO translationDTO) {
        return TranslationDTO.builder()
                .targetText(translationDTO.getSourceText())
                .translatedText(translationDTO.getTranslatedText()).build();
    }


    public static TranslationDAO fromIbmTranslationDTO(TranslationDTO translationDTO) {
        return TranslationDAO.builder()
                .sourceText(translationDTO.getTargetText())
                .translatedText(translationDTO.translatedText).build();
    }
}
